<?php

namespace Database\Factories;

use App\Models\Bill;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\User;

class BillFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model=Bill::class;
    public function definition()
    {

        return [
            'date'=> now(),
            'number' => $this->faker->numberBetween(1,200),
            /* 'total_value' => null,
            'total_tax' => null, */
            'total_value' => $this->faker->numberBetween(1,5000),
            'total_tax' => $this->faker->numberBetween(1,300),
            'observation' => $this->faker->word(100),
            'shipping_adress' => $this->faker->word(50),
            'users_id'=>User::all()->random()->id,

            //
        ];
    }
}
