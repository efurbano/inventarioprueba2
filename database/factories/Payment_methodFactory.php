<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Bill;
use App\Models\Payment_method;

class Payment_methodFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model=Payment_method::class;
    public function definition()
    {

        return [
            //
            'method' => null,
            /* 'method' => $this->faker->enum(1), */
            'value' => $this->faker->numberBetween(1,5000),
            'bills_id' =>Bill::all()->random()->id,

        ];
    }
}
