<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Bill;
use App\Models\Product;
use App\Models\Bills_has_product;

class Bills_has_productFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model=Bills_has_product::class;
    public function definition()
    {
        return [
            //
            'amount' => $this->faker->numberBetween(1,500),
            'bills_id'=> Bill::all()->random()->id,
            'products_id' => Product::all()->random()->id,
        ];
    }
}
