<?php

namespace Database\Factories;

use App\Models\Tax;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class TaxFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model=Tax::class;
    public function definition()
    {
        return [
            'name'=> $this->faker->word(12),
            'type'=> $this->faker->word(7),
            'value'=> $this->faker->numberBetween(0,100),
            //
        ];
    }
}
