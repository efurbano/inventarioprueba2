<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Product;
use App\Models\Tax;
use App\Models\Brand;
use App\Models\Presentation;

class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model=Product::class;
    public function definition()
    {
        return [
            //
            'name'=> $this->faker->word(12),
            'description'=> $this->faker->word(27),
            'price' => $this->faker->numberBetween(1000,20000),
            'stock'=> $this->faker->numberBetween(1,300),
            'type' => $this->faker->word(50),
            'taxes_id'=>Tax::all()->random()->id,
            'brands_id'=>Brand::all()->random()->id,
            'presentations_id'=>Presentation::all()->random()->id,
        ];
    }
}
