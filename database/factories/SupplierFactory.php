<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Supplier;

class SupplierFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model=Supplier::class;
    public function definition()
    {
        return [
            //
            'name'=> $this->faker->word(12),
            'description'=> $this->faker->text(200),
            'direction'=>$this->faker->word(14),
            
            'tel'=>$this->faker->phoneNumber,
        ];
    }
}
