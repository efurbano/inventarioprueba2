<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Products_has_supplier;
use App\Models\Product;
use App\Models\Supplier;

class Products_has_supplierFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model=Products_has_supplier::class;
    public function definition()
    {
        return [
            //
            'expiration' => now(),
            'products_id' => Product::all()->random()->id,
            'suppliers_id' => Supplier::all()->random()->id,
        ];
    }
}
