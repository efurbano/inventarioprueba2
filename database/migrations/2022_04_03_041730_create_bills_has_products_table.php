<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBillsHasProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bills_has_products', function (Blueprint $table) {
            $table->id();
            $table->float('amount');

            $table->unsignedBigInteger('bills_id'); /* foranea de bills*/
            $table->foreign('bills_id')->references('id')->on('bills')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->unsignedBigInteger('products_id'); /* foranea de products */
            $table->foreign('products_id')->references('id')->on('products')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bills_has_products');
    }
}
