<?php
use App\Models\Product;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('description');
            $table->float('price');
            $table->float('stock');
            $table->string('type');
            $table->timestamps();
            $table->unsignedBigInteger('taxes_id'); /* forenea de taxes */
            $table->foreign('taxes_id')->references('id')->on('taxes')
            ->onDelete('cascade')->onUpdate('cascade');

            $table->unsignedBigInteger('brands_id'); /* forenea de brands */
            $table->foreign('brands_id')->references('id')->on('brands')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->unsignedBigInteger('presentations_id'); /* foranea de presentation */
            $table->foreign('presentations_id')->references('id')->on('presentations')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
