<?php

use App\Models\Payment_method;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentMethodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_methods', function (Blueprint $table) {
            $table->id();
            $table->enum('method',[Payment_method::CASH, Payment_method::CARD, Payment_method::BONUS])->default(Payment_method::CASH)->nullable();
            $table->float('value')->nullable;

            $table->unsignedBigInteger('bills_id'); /* forenea de metodos de pago */
            $table->foreign('bills_id')->references('id')->on('bills')
            ->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_methods');
    }
}
