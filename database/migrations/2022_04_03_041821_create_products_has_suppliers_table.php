<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsHasSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    
    public function up()
    {
        Schema::create('products_has_suppliers', function (Blueprint $table) {
            $table->id();
            $table->date('expiration')->nullable();

            $table->unsignedBigInteger('products_id'); /* foranea de products */
            $table->foreign('products_id')->references('id')->on('products')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->unsignedBigInteger('suppliers_id'); /* foranea de suppliers */
            $table->foreign('suppliers_id')->references('id')->on('suppliers')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products_has_suppliers');
    }
}
