<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Payment_method;
use App\Models\Bill;
use App\Models\User;
use App\Models\Tax;
use App\Models\Brand;
use App\Models\Presentation;
use App\Models\Supplier;
use App\Models\Product;
use App\Models\Products_has_supplier;
use App\Models\Bills_has_product;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        User::factory(10)->create();
        Bill::factory(20)->create();
        Payment_method::factory(20)->create();
        Tax::factory(20)->create();
        Brand::factory(20)->create();
        Presentation::factory(20)->create();
        Supplier::factory(20)->create();
        Product::factory(20)->create();
        Products_has_supplier::factory(20)->create();
        Bills_has_product::factory(20)->create();

    }
}
