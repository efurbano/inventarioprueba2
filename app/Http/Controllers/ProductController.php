<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;

class ProductController extends Controller
{
    //
    public function index(){
        $productos = Product::all();
        return $productos;
    }

    public function store(Request $request){
        $product = new Product();
        $product->name = $request->name;
        $product->description = $request->description;
        $product->price = $request->price;
        $product->stock = $request->stock;
        $product->type = $request->type;
        $product->taxes_id = $request->taxes_id;
        $product->brands_id = $request->brands_id;
        $product->brands_id = $request->brands_id;
        $product->presentations_id = $request->presentations_id;
        $product->save();
    }

    public function update(Request $request){
        $product = Product::findOrFail($request->id);
        $product->name = $request->name;
        $product->description = $request->description;
        $product->price = $request->price;
        $product->stock = $request->stock;
        $product->type = $request->type;
        $product->taxes_id = $request->taxes_id;
        $product->brands_id = $request->brands_id;
        $product->brands_id = $request->brands_id;
        $product->presentations_id = $request->presentations_id;
        $product->save();
    }
    public function destroy(Request $request) {
        $product = Product::destroy($request->id);
        return $product;
    }
}
