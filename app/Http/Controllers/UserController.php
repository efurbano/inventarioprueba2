<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Auth\Events\Logout;


class UserController extends Controller
{
    public function register(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required'
        ]);
        
            $user  = new User();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            /* $user->confirm = Hash  $request->confirm; */
            $user->save();
    
            return response()->json([
                'status' => 1,
                'msg' =>  "Regitrado con exito... ",
                'user' => $user 
            ]);
            
        
    }

    public function login(Request $request){
        $request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);
        $user = User::where('email','=',$request->email)->first();
        if(isset ($user->id))
        {
            if(Hash::check($request->password,$user->password)){
                $token = $user->createToken('auth_token')->plainTextToken;
                return response()->json([
                    'status' => 1,
                    'msg' => 'Usuario logeado...',
                    'access_token' => $token
                ]);
            }
            else
            {
                return response()->json([
                    'status' => 0,
                    'msg' => 'Password Incorrecto'
                ]);    
            }
        }
        else
        {
            return response()->json([
                'status' => 0,
                'msg' => 'Usuario no Registrado'
            ]);
        }
    }

    public function userProfile()
    {
        return response()->json([
            'status' => 0,
            'msg' => 'Autenticando usuario',
            'data' => auth()->user()
        ]);
    }

    public function logout()
    {
        auth()->user()->tokens()->delete();
        return response()->json([
            'status' => 0,
            'msg' => 'Cierre de sesion exitosa'
        ]);
    }
}
