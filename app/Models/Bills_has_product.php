<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bills_has_product extends Model
{
    use HasFactory;
    public function bills(){
        return $this->hasMany(Bill::class);
    }
    public function Products(){
        return $this->hasMany(Product::class);
    }
}
