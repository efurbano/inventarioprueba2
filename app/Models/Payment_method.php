<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment_method extends Model
{
    use HasFactory;

    const CASH =1;
    const CARD =2;
    const BONUS =3;
    public function bill(){
        $this->BelongsTo(Bill::class);
    }

}
