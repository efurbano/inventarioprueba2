<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $table="product";
    protected $fillable = [
        name,
        description,
        price,
        stock,
        type,
        taxes_id,
        brands_id,
        presentations_id
    ];

    public function tax(){
        return $this->BelongsTo(Tax::class);
    }

    public function brand(){
        return $this->BelongsTo(Brand::class);
    }
    public function presentation(){
        return $this->BelongsTo(Presentation::class);
    }
    public function bills(){
        return $this->BelongsToMany(Bill::class);
    }
    public function suppliers(){
        return $this->BelongsToMany(Supplier::class);
    }
}
