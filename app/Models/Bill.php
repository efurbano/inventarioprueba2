<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
    use HasFactory;
    public function user() {
        return $this->BelongsTo(User::class);
    }
    public function products(){
        return $this->BelongsToMany(Product::class);
    }
    public function payment_methods(){
        return $this->BelongsToMany(Payment_method::class);
    }
}
