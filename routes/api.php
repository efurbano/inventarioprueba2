<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
}); */

Route::post('register',[App\Http\Controllers\UserController::class,'register']);
Route::post('login',[App\Http\Controllers\UserController::class,'login']);


Route::group(['middleware' => ['auth:sanctum']],function ()
    {
        Route::get('userprofile',[App\Http\Controllers\UserController::class,'userProfile']);
        Route::get('logout',[App\Http\Controllers\UserController::class,'logout']);
    });
Route::get('/product',[App\Http\Controllers\ProductController::class,'index']);
Route::post('/product',[App\Http\Controllers\ProductController::class,'store']);
Route::put('/product/{id}',[App\Http\Controllers\ProductController::class,'update']);
Route::delete('/product/{id}',[App\Http\Controllers\ProductController::class,'destrpy']);




